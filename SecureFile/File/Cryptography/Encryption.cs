﻿using System;

namespace SecureFile.File.Cryptography
{
    public class Encryption
    {
        /// <summary>
        /// Secure file
        /// </summary>
        /// <param name="data">Data should be secured</param>
        /// <param name="hex">The algorithm for changing</param>
        public void SecureFile(ref byte[] data, string hex)
        {
            for (var i = 0; i < data.Length; i++) data[i] = (byte)(data[i] ^ HexToInt(hex));
        }

        /// <summary>
        /// Make Hexadecimal number into Integer
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        private static int HexToInt(string hex) => Convert.ToInt32(hex, 16);
    }
}
