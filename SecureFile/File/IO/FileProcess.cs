﻿using System.Threading.Tasks;

namespace SecureFile.File.IO
{
    public class FileProcess
    {
        /// <summary>
        /// Read all bytes related to file
        /// </summary>
        /// <param name="path">input file path</param>
        /// <returns></returns>
        public async Task<byte[]> ReadAllBytesAsync(string path) => await System.IO.File.ReadAllBytesAsync(path);

        /// <summary>
        /// Write processed bytes in file
        /// </summary>
        /// <param name="path">output file path</param>
        /// <param name="data">bytes</param>
        /// <returns></returns>
        public async Task WriteAllBytesAsync(string path, byte[] data) => await System.IO.File.WriteAllBytesAsync(path, data);
    }
}
