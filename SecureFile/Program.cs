﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SecureFile.File.Cryptography;
using SecureFile.File.IO;

namespace SecureFile
{
    public static class Program
    {
        public static async Task Main(string[] args)
        {
            if (args.Length < 1 && args.Length > 3)
            {
                Console.WriteLine("Enter invalid inputs ...");
                return;
            }

            var (input, output, hex) = LaunchParams(args);

            var file = new FileProcess();

            var data = await file.ReadAllBytesAsync(input);

            var encryption = new Encryption();

            encryption.SecureFile(ref data, hex);

            await file.WriteAllBytesAsync(!string.IsNullOrEmpty(output) ? output : input, data);
        }

        /// <summary>
        /// Guess the input params from console
        /// </summary>
        /// <param name="args">console params</param>
        /// <returns></returns>
        private static (string inputPath, string outputPath, string hex) LaunchParams(IReadOnlyList<string> args)
        {
            string input = "", output = "", hex = "0X60";

            const string windowsPattern =
                @"(^([a-z]|[A-Z]):(?=\\(?![\0-\37<>:""/\\|?*])|\/(?![\0-\37<>:""/\\|?*])|$)|^\\(?=[\\\/][^\0-\37<>:""/\\|?*]+)|^(?=(\\|\/)$)|^\.(?=(\\|\/)$)|^\.\.(?=(\\|\/)$)|^(?=(\\|\/)[^\0-\37<>:""/\\|?*]+)|^\.(?=(\\|\/)[^\0-\37<>:""/\\|?*]+)|^\.\.(?=(\\|\/)[^\0-\37<>:""/\\|?*]+))((\\|\/)[^\0-\37<>:""/\\|?*]+|(\\|\/)$)*()$";
            const string unixPattern = @"^\/$|(^(?=\/)|^\.|^\.\.)(\/(?=[^/\0])[^/\0]+)*\/?$";

            for (var i = 0; i < args.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        input = args[0];
                        break;
                    case 1:
                        {

                            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                            {
                                if (Regex.IsMatch(args[1], windowsPattern))
                                {
                                    output = args[1];
                                    break;
                                }

                                hex = args[1];
                                break;
                            }

                            if (Regex.IsMatch(args[1], unixPattern))
                            {
                                output = args[1];
                                break;
                            }

                            hex = args[1];
                            break;
                        }
                    default:
                        hex = args[2];
                        break;
                }
            }

            return (input, output, hex);
        }
    }
}